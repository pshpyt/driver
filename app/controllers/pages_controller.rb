class PagesController < ApplicationController
 # before_filter :authenticate_user!
  helper :headshot
  
  def home
  
  end

  def print_page
    render :layout => false
  end
  
  def view_page
   cookies[:person_id] = params[:person_id]
   render 'resume/print_resume'
  end
end
